<?php

namespace CodeFin\Repositories;

trait GetClientsTrait{
    private function getClients(){
        /** @var TYPE_NAME $repository */
        $repository = app(\CodeFin\Repositories\ClientRepository::class);
        $repository->skipPresenter(true);
        return $repository->all();
    }

}


<?php

namespace CodeFin\Events;


use CodeFin\Models\Bank;

class BankCreatedEvent
{
    private $bank;

    /**
     * Create a new event instance.
     *
     * @param Bank $bank
     */
    public function __construct(Bank $bank)
    {
        //
        $this->bank = $bank;
    }

    public function getBank()
    {
        return $this->bank;
    }


}
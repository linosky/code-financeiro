<?php
namespace CodeFin\Http\Controllers\Api;
use CodeFin\Repositories\CategoryExpenseRepository;
use CodeFin\Http\Controllers\Controller;
use CodeFin\Criteria\WithDepthCategoriesCriteria;

class CategoryExpensesController extends Controller {

    use CategoriesControllerTrait;

    protected $repository;

    /**
     * CategoryRevenuesController constructor.
     * @param CategoryExpenseRepository $repository
     */
    public function __construct(CategoryExpenseRepository $repository) {
        $this->repository = $repository;
        $this->repository->pushCriteria(new WithDepthCategoriesCriteria());
    }
}
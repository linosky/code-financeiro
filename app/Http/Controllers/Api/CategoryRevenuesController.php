<?php
namespace CodeFin\Http\Controllers\Api;
use CodeFin\Repositories\CategoryRevenueRepository;
use CodeFin\Http\Controllers\Controller;
use CodeFin\Criteria\WithDepthCategoriesCriteria;
class CategoryRevenuesController extends Controller {
    use CategoriesControllerTrait;
    protected $repository;
    public function __construct(CategoryRevenueRepository $repository) {
        $this->repository = $repository;
        $this->repository->pushCriteria(new WithDepthCategoriesCriteria());
    }
}
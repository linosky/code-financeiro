@extends('layouts.admin')

@section('content')
    <div class="container">
        <div class="row">
            <h5>Minhas contas</h5>
            {!! Form::model($bank, [
                    'route' => ['admin.banks.update', 'bank' => $bank->id],
                    'method' => 'PUT',
                    'files' => true
             ]) !!}
            @include('admin.banks._form')
            <div class="row">
                {!! Form::submit('Salvar banco', ['class' => 'btn waves-effect']) !!}
            </div>
            {!! Form::close() !!}
        </div>
    </div>
@endsection
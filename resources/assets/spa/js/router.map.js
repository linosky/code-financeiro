import LoginComponent               from './components/Login.vue';
import DasboardComponent            from './components/Dashboard.vue';
import LogoutComponent              from './components/Logout.vue';
import BankAccountListComponent     from './components/bank-account/BankAccountList.vue';
import BankAccountCreateComponent   from './components/bank-account/BankAccountCreate.vue';
import BankAccountUpdateComponent   from './components/bank-account/BankAccountUpdate.vue';
import CategoryListComponent        from './components/category/CategoryList.vue';
import PlanAccountComponent         from './components/category/PlanAccount.vue';
import BillPayComponent             from './components/bill/bill-pay/BillPayList.vue';


export default {
    '/login' : {
        name: 'auth.login',
        component: LoginComponent,
        auth: false
    },
    '/logout' : {
        name: 'auth.logout',
        component: LogoutComponent,
        auth: true
    },
    '/dashboard' : {
        name: 'dashboard',
        component: DasboardComponent,
        auth: true
    },
    '/bank-accounts':{
        component: {template: "<router-view></router-view>"},
        subRoutes: {
            '/': {
                name: "bank-account.list",
                component: BankAccountListComponent,
                auth: true
            },
            '/:id/update': {
                name: "bank-account.update",
                component: BankAccountUpdateComponent,
                auth: true
            },
            '/create': {
                name: "bank-account.create",
                component: BankAccountCreateComponent,
                auth: true
            }
        }
    },
    '/categories': {
        name: 'category.list',
        component: CategoryListComponent,
        auth: true
    },
    '/plan-account': {
        name: 'plan-account.list',
        component: PlanAccountComponent,
        auth: true
    },
    '/bill-pay':{
        component: {template: "<router-view></router-view>"},
        subRoutes: {
            '/': {
                name: "bill-pay.list",
                component: BillPayComponent,
                auth: true
            },
            '/:id/update': {
                name: "bank-account.update",
                component: BankAccountUpdateComponent,
                auth: true
            },
            '/create': {
                name: "bank-account.create",
                component: BankAccountCreateComponent,
                auth: true
            }
        }
    },
}
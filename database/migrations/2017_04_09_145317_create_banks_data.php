<?php

use CodeFin\Repositories\BankRepository;
use Illuminate\Http\UploadedFile;

use Illuminate\Database\Migrations\Migration;

class CreateBanksData extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        /** @var BankRepository $repository */
        $repository = app(BankRepository::class);
        foreach ($this->getData() as $bankArray){
            $repository->create($bankArray);
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        $repository = app(BankRepository::class);
        $repository->skipPresenter(true);
        $count = count($this->getData());
        foreach (range(1, $count) as $value){
            $model = $repository->find($value);
            $path = \CodeFin\Models\Bank::logosDIr().'/'.$model->logo;
            \Storage::disk('public')->delete($path);
            $model->delete();
        }
    }

    public function getData()
    {
        return [
            [ 'name' => 'Caixa', 'logo' => new UploadedFile( storage_path('app/files/banks/logos/icone_caixa.jpg'), 'icone_caixa.jpg') ],
            [ 'name' => 'Bradesco', 'logo' => new UploadedFile( storage_path('app/files/banks/logos/icone_bradesco.jpg'), 'icone_bradesco.jpg') ],
            [ 'name' => 'Banco do Brasil', 'logo' => new UploadedFile( storage_path('app/files/banks/logos/icone_bb.jpg'), 'icone_bb.jpg') ],
            [ 'name' => 'Itau', 'logo' => new UploadedFile( storage_path('app/files/banks/logos/icone_itau.jpg'), 'icone_itau.jpg') ],
            [ 'name' => 'Santander', 'logo' => new UploadedFile( storage_path('app/files/banks/logos/icone_santander.jpg'), 'icone_santander.jpg') ],
            [ 'name' => 'HSBC', 'logo' => new UploadedFile( storage_path('app/files/banks/logos/icone_hsbc.jpg'), 'icone_hsbc.jpg') ],
            [ 'name' => 'SICOOB', 'logo' => new UploadedFile( storage_path('app/files/banks/logos/icone_sicoob.jpg'), 'icone_sicoob.jpg') ],
        ];
    }
}
